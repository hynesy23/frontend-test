# React Pokedex

This is a SPA created using create-react-app. It makes a GET request to the Pokeapi to get all Pokemon, and a second request to get a single pokemon bu their name.

## Getting Started

The instructions below will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

To run this locally you will need Node.js and git installated on your system.

First check if you have node.js installed with below command:

```bash
node -v
```

If you do not have Node.js installed or command above does not work please follow the instructions on this guide: https://nodejs.org/en/download/package-manager/

Finally, check if git is installed on your machine enter the following command on your terminal:

```bash
git --version
```

To make calls to the Pokemon API, you do not require a key, or authentication. Simply direct your GET requests to https://pokeapi.co/api/v2/, followed by the resource you wish to obtain. More info can be found on their site, https://pokeapi.co/.

### Installation/Running local version

Now that the prerequisites have been installed you can now install and run this application.

First you will need to clone this repo, to do so use the command line to navigate to your preferred directory on your local machine and enter the following command on the terminal:

```bash
git clone https://gitlab.com/hynesy23/frontend-test
```

Navigate inside the folder and install all dependencies by entering the following commands on your terminal window:

```bash
cd frontend-test

npm install
```

This React App is nested within the original repo, under react-pokedex. To run the application, cd into this folder and enter the following command in your terminal window:

```
npm start
```

This will run the server on port 3000 and open the webpage in your browser or you can navigate to http://localhost:3000 in your browser manually.

### Compiling Production

To compilce and deploy, in the react-pokedex folder, run the following command:

```
npm run build
```

## Author

Cillian Hynes

## Contributing

This project is a tech test for UKFast and is not accepting contributions.

## Acknowledgments

This website was created as part of a tech test for UKFast. Data was obtained from https://pokeapi.co/. All trademarks as the property of their respective owners.
