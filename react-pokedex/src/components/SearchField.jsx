import React, { Component } from "react";

export default class SearchField extends Component {
  state = {
    searchTerm: ""
  };

  handleChange = event => {
    const searchTerm = event.target.value;
    this.setState({ searchTerm });
  };

  handleSubmit = event => {
    event.preventDefault();
    const { searchTerm } = this.state;
    this.props.showSinglePokemon(searchTerm);
    this.setState({ searchTerm: "" });
  };

  render() {
    return (
      <>
        <form onSubmit={this.handleSubmit} className="search">
          <input
            type="text"
            placeholder="Search"
            onChange={this.handleChange}
            value={this.state.searchTerm}
            className="search"
          />
        </form>
      </>
    );
  }
}
