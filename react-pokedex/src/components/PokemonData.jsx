import React, { Component } from "react";
import DataTable from "./DataTable";

import * as api from "../api/api";

export default class PokemonData extends Component {
  state = {
    pokemon: null
  };

  componentDidMount() {
    const { pokemon } = this.props;
    api.getPokemonByName(pokemon).then(pokemon => {
      this.setState({ pokemon });
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps !== this.props) {
      const { pokemon } = this.props;
      api.getPokemonByName(pokemon).then(pokemon => {
        this.setState({ pokemon });
      });
    }
  }

  render() {
    const { pokemon } = this.state;
    if (pokemon) {
      return (
        <div className="poke-data">
          <img
            src={pokemon.sprites.front_default}
            alt="image of pokemon"
            className="image"
          />
          <DataTable pokemon={pokemon} />
        </div>
      );
    } else {
      return null;
    }
  }
}
