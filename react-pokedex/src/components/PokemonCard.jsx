import React from "react";

export default function PokemonCard({ pokemon }) {
  return (
    <p className="poke-card">{`${pokemon.name[0].toUpperCase()}${pokemon.name.slice(
      1,
      pokemon.name.length
    )}`}</p>
  );
}
