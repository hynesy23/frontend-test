import React from "react";

export default function Error() {
  return (
    <p className="error">
      Uh oh, there are no Pokemon by that name. Please try again
    </p>
  );
}
