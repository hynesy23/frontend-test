import React from "react";

export default function DataTable({ pokemon }) {
  return (
    <table className="table" cellPadding="10">
      <thead>
        <tr>
          <th>{`${pokemon.name[0].toUpperCase()}${pokemon.name.slice(
            1,
            pokemon.name.length
          )}`}</th>
          <th className="abilities">Abilities:</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td className="abilities">Weight: {pokemon.weight}</td>
          <td>
            {pokemon.abilities.map((ability, i) => {
              return (
                <button className="ability" key={i}>
                  {ability.ability.name}
                </button>
              );
            })}
          </td>
        </tr>
        <tr>
          <td className="abilities">Height: {pokemon.height}</td>
        </tr>
      </tbody>
    </table>
  );
}
