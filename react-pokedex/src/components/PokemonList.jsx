import React, { Component } from "react";
import * as api from "../api/api";
import PokemonCard from "./PokemonCard";
import Pagination from "./Pagination";

export default class PokemonList extends Component {
  state = {
    pokemons: [],
    elements: [],
    isLoading: true,
    offset: 0,
    currentPage: 0,
    perPage: 17,
    pageCount: 0
  };

  componentDidMount() {
    api.getAllPokemon().then(pokemons => {
      this.setState(
        {
          pokemons,
          isLoading: false,
          pageCount: Math.ceil(pokemons.length / this.state.perPage)
        },
        () => this.setElementsForCurrentPage()
      );
    });
  }

  setElementsForCurrentPage = () => {
    let { perPage, pokemons, offset } = this.state;
    const elements = pokemons.slice(offset, offset + perPage);
    this.setState({ elements, isLoading: false });
  };

  handlePageClick = data => {
    let { offset, perPage } = this.state;
    const selectedPage = data.selected;
    offset = selectedPage * perPage;
    this.setState({ currentPage: selectedPage, offset }, () =>
      this.setElementsForCurrentPage()
    );
  };

  render() {
    const { elements, isLoading, pageCount, currentPage } = this.state;
    if (isLoading) return <p>Loading, please wait</p>;
    return (
      <>
        <ul className="poke-list">
          <li className="title">Pokedex</li>
          <li>List of Pokemon</li>
          {elements.map(pokemon => {
            return <PokemonCard pokemon={pokemon} key={pokemon.name} />;
          })}
          <Pagination
            pageCount={pageCount}
            currentPage={currentPage}
            handlePageClick={this.handlePageClick}
          />
        </ul>
      </>
    );
  }
}
