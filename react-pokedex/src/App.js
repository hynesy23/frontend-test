import React, { Component } from "react";
import "./App.css";
import PokemonList from "./components/PokemonList";
import SearchField from "./components/SearchField";
import PokemonData from "./components/PokemonData";
import Error from "./components/Error";
import * as api from "./api/api";

class App extends Component {
  state = {
    result: null,
    err: false
  };

  showSinglePokemon = name => {
    api
      .getPokemonByName(name)
      .then(result => {
        this.setState({ result, err: false });
      })
      .catch(err => {
        this.setState({ err: true });
      });
  };

  render() {
    const { result, err } = this.state;
    // if (err) return <p>Uh oh, looks like there's been an error</p>;
    return (
      <div className="App">
        <PokemonList />
        <SearchField showSinglePokemon={this.showSinglePokemon} />
        {err && <Error />}
        <PokemonData pokemon={result} />
      </div>
    );
  }
}

export default App;
