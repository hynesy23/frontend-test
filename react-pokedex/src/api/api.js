import axios from "axios";

const baseURL = "https://pokeapi.co/api/v2/pokemon";

export const getAllPokemon = () => {
  return axios.get(`${baseURL}?limit=1000`).then(({ data }) => {
    return data.results;
  });
};

export const getPokemonByName = name => {
  return axios.get(`${baseURL}/${name}`).then(({ data }) => {
    return data;
  });
};
